import secrets
import string
import os


password_length = int(os.environ.get("PASSWORD_LENGTH"))
print(f'PASSWORD_LENGTH = {password_length}')


def generate_password(length: int) -> str:
    if length < 8 or length > 32:
        return "Длина пароля должна быть от 8 до 32 символов"
    else:
        alphabet = string.ascii_letters + string.digits + string.punctuation
        password = ''.join(secrets.choice(alphabet) for _ in range(length))
        return password


password = generate_password(password_length)
print(password)
