CREATE TABLE IF NOT EXISTS Student (
    id SERIAL PRIMARY KEY,
    last_name VARCHAR(255),
    first_name VARCHAR(255),
    surname VARCHAR(255),
    birth_date DATE
);


CREATE TABLE IF NOT EXISTS SubjectExam (
    id SERIAL PRIMARY KEY,
    student_id INT REFERENCES Student(id),
    name VARCHAR(255),
    exam_date DATE,
    teacher_name VARCHAR(255)
);

INSERT INTO Student (last_name, first_name, surname, birth_date)
VALUES
    ('Иванов', 'Иван', 'Иванович', '1990-01-01'),
    ('Петрова', 'Мария', 'Геннадьевна', '1992-05-20'),
    ('Смирнов', 'Алексей', 'Павлович', '1991-11-05'),
    ('Иванова', 'Елена', 'Игоревна', '1993-08-15'),
    ('Козлов', 'Артем', 'Сергеевич', '1990-04-30');

INSERT INTO SubjectExam (student_id, name, exam_date, teacher_name)
VALUES
    ('1', 'Физика', '1990-01-01', 'Иванова А.Г.'),
    ('3', 'Высшая математика', '1993-08-15', 'Паскаль В.В.'),
    ('3', 'Информатика', '1992-04-12', 'Ковальчук О.И.');
