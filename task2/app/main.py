import psycopg2
conn = psycopg2.connect(database="students_db", user="postgres",
                        password="postgres", host="db", port=5432)

cursor = conn.cursor()

cursor.execute("""
               SELECT last_name, first_name, surname
                FROM Student
                WHERE birth_date=(SELECT MIN(birth_date) FROM Student);
               """)


print(f"Самый старший ученик: {' '.join(cursor.fetchone())}")

cursor.execute("""
               SELECT last_name, first_name, surname
                FROM Student
                WHERE birth_date=(SELECT MAX(birth_date) FROM Student);
               """)

print(f"Самый младший ученик: {' '.join(cursor.fetchone())}")

cursor.close()
conn.close()
